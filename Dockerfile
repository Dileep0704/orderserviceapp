FROM openjdk:8-jdk-alpine

LABEL maintainer="Pradeep Kumar L"

VOLUME /tmp

ARG JAR_FILE

COPY ${JAR_FILE} app.jar

EXPOSE 8222

ENTRYPOINT ["java", "-jar", "/app.jar"]