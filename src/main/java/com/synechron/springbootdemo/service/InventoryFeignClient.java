package com.synechron.springbootdemo.service;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;

@FeignClient("inventoryservice")
public interface InventoryFeignClient {

    @GetMapping(value = "/api/v1/inventory", consumes = APPLICATION_JSON_VALUE)
    public int fetchItemsCount();

    @PostMapping(value = "/api/v1/inventory", consumes = APPLICATION_JSON_VALUE)
    public ResponseEntity<Integer> updateQty();
}